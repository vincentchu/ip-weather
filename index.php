<?php 
//API Keys
$darkskyapi = "1234";
$googlemapsapi = "1234";
	
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<title>What is my IP Address?</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="quickbrownfoxes.org IP Address lookup and weather.">
	<meta property="og:image" content="../../img/avatar.png" />
	<meta name="theme-color" content="#ff7f2a">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha256-eSi1q2PG6J7g7ib17yAaWMcrr5GrtohYChqibrV7PBE=" crossorigin="anonymous" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="shortcut icon" href="favicon.ico" />


</head>


<body>
	<div class="darker">
		<div class="container text-white">
			<div class="row">
				<div class="col-12 text-center">
					<br />
					<h2><b>The IP address of your computer.</b></h2>
					<h2 class="text-truncate">
						<?php
						$ip = $_SERVER["HTTP_CF_CONNECTING_IP"];
						function ipinfo($url)
						{
							$ch = curl_init();  

							curl_setopt($ch,CURLOPT_URL,$url);
							curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
							$output=curl_exec($ch);

							curl_close($ch);
							$details = json_decode($output);
							return $details;
						}
					
						$details = ipinfo("https://ipinfo.io/{$ip}/json");
						echo $ip;
					?>

					</h2>
					<br />
				</div>

			</div>
			<div class="row">
				<div class="col-md-6">
					<h3 class="text-center">Details</h3>
					<hr class="text-white" />
					<b>Hostname:</b>
					<?php echo $details->hostname; ?>

					<br />
					<b>ISP:</b>
					<?php echo $details->org; ?>

					<br />
					<b>Country:</b>
					<?php echo $details->country; ?>

					<br />
					<b>City:</b>
					<?php echo $details->city; ?>

					<br />
					<b>State:</b>
					<?php echo $details->region; ?>

					<br />
					<b>Zipcode:</b>
					<?php echo $details->postal; ?>

					<br />
					<br />
					<p>If you change your IP close out of your browser and revisit this page.</p>
					<br />
					<br />
				</div>

				<div class="col-md-6">
					<h3 class="text-center">Map</h3>
					<hr class="text-white" />
					<div class="embed-responsive embed-responsive-4by3">
						<iframe class="embed-responsive-item" width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $details->loc; ?>&zoom=5&key=<?php echo $googlemapsapi; ?>" allowfullscreen>
						</iframe>
					</div>
				</div>
			</div>
			<br />
			<?php
			function weather($url)
			{
				$ch = curl_init();  

				curl_setopt($ch,CURLOPT_URL,$url);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
				$output=curl_exec($ch);

				curl_close($ch);
				$data = json_decode($output);
				return $data;
			}

			$data = weather("https://api.darksky.net/forecast/{$darkskyapi}/{$details->loc}");
		?>
			<h2 class="text-center">Weather Information</h2>
			<hr />
			<div class="row">
				<div class="col-md-6">
					<p><strong>Right now</strong> in
						<?php echo $details->city; ?>,
						<?php echo $details->region; ?> it is
						<?php echo $data->currently->temperature; ?>°F with a
						<?php echo ($data->currently->precipProbability * 100) . '%'; ?> chance of rain.
						<br /><br />

						<strong>Today:</strong><br />
						<?php echo $data->hourly->summary; ?>
						<br /><br />

						<strong>This Week:</strong><br />
						<?php echo $data->daily->summary; ?>
						<br /><br />

					</p>
				</div>
				<div class="col-md-2">
					<canvas id="<?php echo $data->currently->icon; ?>" width="128" height="128"></canvas>
					<br />
					<p class="d-block d-md-none">
						<strong><?php echo $data->currently->summary; ?></strong>
					</p>
					<p class="text-center d-none d-md-block">
						<strong><?php echo $data->currently->summary; ?></strong>
					</p>
				</div>
				<div class="col-md-3">
					<strong>Inches of rain:</strong>
					<?php echo $data->currently->precipIntensity; ?>
					<br />
					<strong>Humidity:</strong>
					<?php echo ($data->currently->humidity * 100) . '%'; ?>
					<br />
					<strong>Feels Like:</strong>
					<?php echo $data->currently->apparentTemperature; ?>°F
					<br />
					<strong>Visibility:</strong>
					<?php echo $data->currently->visibility; ?> Miles
					<br />
					<strong>Wind Speed:</strong>
					<?php echo $data->currently->windSpeed; ?> MPH
					<br />
					<strong>Wind Gust:</strong>
					<?php echo $data->currently->windGust; ?> MPH
					<br />
					<strong>UV Index:</strong>
					<?php echo $data->currently->uvIndex; ?>
					<br />
				</div>
			</div>
		</div>
	</div>
	<br />

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/skycons/1396634940/skycons.js" integrity="sha256-nCLOiEB2E5Mk++5XNR0otwdCPrAteym9WaDVgKhtrE0=" crossorigin="anonymous"></script>
	<script src="js/icons.js"></script>
	<script src="js/bg.js"></script>

</body>

</html>
